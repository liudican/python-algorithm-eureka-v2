class CommonResponse(object):
    def __init__(self, code, msg, data):
        self.code = code
        self.msg = msg
        self.data = data

class SiteData(object):
    def __init__(self, stationId, value):
        self.stationId = stationId
        self.value = value

# 执行成功的状态码
successCode = "0"
# 执行成功的msg
successMsg = ""
# 执行失败的状态码
errorCode = "1"
# 缺测值
missingValues = -999.0
# 超时信息
requestTimeoutMsg = "requestTimeout"
# 日志路径及日志名称
defaultLogName = "info.log"
logPath = "/data/logs/tainzi-cq/python-test/"
# 超时时间(单位秒，算法最长时间设置为超过10分钟报错)
timeoutTime = 600
# 最大进程数(算法个数以上，内核数(如果内核是超线程再*2)以下个数)，我们算法数是10个+出图2个【不能为1，为1会出问题】
maxProcess = 4

