#需要特别注意很多依赖包要满足要素

   numpy>=1.12.1
   scipy>=0.19.0
   pandas>=0.20.0
   xarray>=0.11.0
   scikit-learn>=0.21.2
   matplotlib>=3.3.1
   httplib2>=0.12.0
   protobuf>=3.6.1
   netCDF4>=1.4.2
   pyshp>=2.1.0
   seaborn>=0.9.0
   ipywidgets>=7.5.1
