import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.web import RequestHandler
from tornado.options import define, options
import py_eureka_client.eureka_client as eureka_client
from interpSgCressman import InterpSgCressmanHandler
from logHandler import getLogger
import os
import common as com

# 10.172.16.59
# 192.168.1.133
# 192.168.1.161
# 10.230.124.147
define("eurekaServer", default="http://10.230.124.147:8761/eureka/", help="eureka服务器地址", type=str)
define("appName", default="python-test", help="注册应用名称", type=str)
define("port", default=8081, help="程序端口号", type=int)

class InfoHandler(RequestHandler):
    def get(self):
        self.write("hello python eureka!")

def eurekaClient():
    # 判断日志目录是否存在，不存在则创建
    log_path = com.logPath
    if os.path.exists(log_path) is False:
        os.makedirs(log_path)

    tornado.options.parse_command_line()
    log = getLogger()
    log.info("eureka开始启动")
    eureka_client.init_registry_client(eureka_server=options.eurekaServer,
                                       app_name=options.appName,
                                       instance_port=options.port)
    log.info("eureka启动成功")
    app = tornado.web.Application(
        handlers=[
            (r"/info", InfoHandler),
            (r"/interpSgCressman", InterpSgCressmanHandler)
        ],
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    http_server.start(com.maxProcess)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    eurekaClient()