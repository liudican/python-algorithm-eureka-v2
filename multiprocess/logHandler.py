#!/usr/bin/env python
# -*-  coding:utf-8 -*-

import logging
import common as com
from cloghandler import ConcurrentRotatingFileHandler
# pip install ConcurrentLogHandler

def getLogger():
    log_name = com.logPath + com.defaultLogName
    logger = logging.getLogger(log_name)
    if not logger.handlers:
        # 20M一个文件，最多保留5个
        handler = ConcurrentRotatingFileHandler(log_name, "a", 20 * 1024 * 1024, 5)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] - %(filename)s [Line:%(lineno)d] - [%(levelname)s]-[thread:%(thread)s]-[process:%(process)s] - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    return logger