import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.web import RequestHandler
from tornado.options import define, options
import time
import os
import py_eureka_client.eureka_client as eureka_client
from logHandler import getLogger
import common as com
from timeoutHandler import set_timeout

define("eurekaServer", default="http://192.168.1.133:8761/eureka/", help="eureka服务器地址", type=str)
define("appName", default="python-multiprocess", help="注册应用名称", type=str)
define("port", default=8002, help="程序端口号", type=int)

class InfoHandler(RequestHandler):
    def get(self):
        log = getLogger()
        log.info("hello python eureka!")
        self.write("hello python eureka!")

class NonBlockingHandler(RequestHandler):
    def get(self):
        log = getLogger()
        log.info("开始睡眠20秒")
        time.sleep(20)
        log.info("睡眠结束!")
        self.write("睡眠结束!")

class TimeOutTestHandler(RequestHandler):
    # 测试5秒超时
    @set_timeout(5)
    def get(self):
        try:
            log = getLogger()
            log.info("测试超时开始睡眠20秒")
            time.sleep(20)
            log.error("测试超时失败!")
            self.write("测试超时失败!")
        except BaseException as e:
            log.info("测试超时成功!")
            self.write("测试超时成功!")


def eurekaClient():
    # 判断日志目录是否存在，不存在则创建
    log_path = com.logPath
    if os.path.exists(log_path) is False:
        os.makedirs(log_path)

    tornado.options.parse_command_line()
    log = getLogger()
    log.info("eureka开始启动")
    eureka_client.init_registry_client(eureka_server=options.eurekaServer,
                                       app_name=options.appName,
                                       instance_port=options.port)
    log.info("eureka启动成功")
    app = tornado.web.Application(
        handlers=[
            (r"/info", InfoHandler),
            (r"/nonBlockingHandler", NonBlockingHandler),
            (r"/timeOutTestHandler", TimeOutTestHandler)
        ],
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    # 设置开的进程个数
    http_server.start(4)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    eurekaClient()