#!/usr/bin/env python
# -*-  coding:utf-8 -*-

import logging
import common as com

def getLogger():
    log_name = com.logPath + com.defaultLogName
    logger = logging.getLogger(log_name)
    if not logger.handlers:
        # 只保留5天内的
        handler = logging.handlers.TimedRotatingFileHandler(log_name, 'D', 1, 5)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] - %(filename)s [Line:%(lineno)d] - [%(levelname)s]-[thread:%(thread)s]-[process:%(process)s] - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    return logger