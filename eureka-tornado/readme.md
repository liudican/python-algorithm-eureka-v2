#python算法
##该项目时cpu密集型且执行时间较长，实时采集预处理时有一定并发，需要解决效率需求，其余时候并发不高
##使用的web框架是tornado，并使用eureka来管理服务，由于tornado默认是单进程和单线程的同步阻塞请求，且基于项目需求，我们还需要使用多进程技术
##虽然每个算法执行完都要生成图片，但是生成图片不是并发执行的，每个算法执行时间长度不一样，所以svg也不用加多线程技术

##1.日志模块logHandler.py
### 使用TimedRotatingFileHandler处理日志，日志输出到txt中，每天的打一个包，并自动删除5天前的
### 注意：每次获取logger时，不要重复廷加handler，不然会造成OSError: [Errno 24] Too many open files错误

##2.使用多进程
#### 设置开的进程个数，http_server.start(n),n个进程
#### 启动多进程之后，查看ps -ef|grep python 会一共看到n+1ge eurekaClient进程，其中n个子进程，1个父进程
#### 不同的方法可以异步执行；相同的方法时，要用不同的浏览器测试，也是可以的（同一个浏览器，不能异步执行相同的方法，可能是因为请求相同且会话相同时就要排队）

##3.signal函数超时设置
### 注意：timeoutHandle模块，不能在多线程中使用，只能在主线程中接收信号

##4.异常解决
###4.1 ImportError: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found
### 解决（亲测有效）：https://blog.csdn.net/qq_29695701/article/details/86292961

###5.未解决问题
###5.1 svg.py出图，非常小的概率还是会被阻塞，设置的函数超时并没有生效