import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.web import RequestHandler
from tornado.options import define, options
import py_eureka_client.eureka_client as eureka_client
from indexKMeans import IndexKMeansHandler
from bayes import NaiveBayesHandler
from decisionTree import DecisionTreeHandler
from svm_test import SvmTestHandler
from svm_train import SvmTrainHandler
from mgf import MgfHandler
from multipleRegression import MultipleRegressionHandler
from optimalSubset import OptimalSubsetHandler
from rhythm import RhythmHandler
from pca import PcaHandler
from svg import SvgImageHandler
from svgNoInterpolation import SvgNoInterpolationImageHandler
from tcc import TccSvgHandler
from vectorWind import VectorWindHandler
from logHandler import getLogger
import os
import common as com

# 10.172.16.59
# 192.168.1.133
# 192.168.1.161
define("eurekaServer", default="http://192.168.1.161:8761/eureka/", help="eureka服务器地址", type=str)
define("appName", default="python", help="注册应用名称", type=str)
define("port", default=8001, help="程序端口号", type=int)

class InfoHandler(RequestHandler):
    def get(self):
        self.write("hello python eureka!")

def eurekaClient():
    # 判断日志目录是否存在，不存在则创建
    log_path = com.logPath
    if os.path.exists(log_path) is False:
        os.makedirs(log_path)

    tornado.options.parse_command_line()
    log = getLogger()
    log.info("eureka开始启动")
    eureka_client.init_registry_client(eureka_server=options.eurekaServer,
                                       app_name=options.appName,
                                       instance_port=options.port)
    log.info("eureka启动成功")
    app = tornado.web.Application(
        handlers=[
            (r"/info", InfoHandler),
            (r"/kmeans", IndexKMeansHandler),
            (r"/naiveBayes", NaiveBayesHandler),
            (r"/decisionTree", DecisionTreeHandler),
            (r"/createSvmModel", SvmTrainHandler),
            (r"/svm", SvmTestHandler),
            (r"/mgf", MgfHandler),
            (r"/multipleRegression", MultipleRegressionHandler),
            (r"/optimalSubset", OptimalSubsetHandler),
            (r"/rhythm", RhythmHandler),
            (r"/pca", PcaHandler),
            (r"/createSvgImage", SvgImageHandler),
            (r"/createSvgNoInterpolationImage", SvgNoInterpolationImageHandler),
            (r"/tcc", TccSvgHandler),
            (r"/createVectorWindImage", VectorWindHandler)
        ],
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    http_server.start(com.maxProcess)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    eurekaClient()