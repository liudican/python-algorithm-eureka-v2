for p_pid in `ps -ef |grep eurekaClient | egrep -v grep | awk '{print $2"-"$3}'`
do
  # 子pid
	pid1=${p_pid%-*}
	# 父pid
	pid2=${p_pid#*-}
	# 1.先删除父进程
	if [ $pid2 -eq "1" ]
	then
		kill -9  $pid1
	fi
done
for sub_pid in `ps -ef |grep eurekaClient | egrep -v grep | awk '{print $2}'`
do
  # 2.再删除子进程
	kill -9 $sub_pid
done